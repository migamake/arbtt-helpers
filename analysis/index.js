const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000
const path = require('path')
const {exec} = require('child_process')
var dp = require(path.join(__dirname+"/nodesrc/dataProcess.js"))

app.use(cors())
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/index.html'))
})

function arbttShellCommand(){
  return 'arbtt-stats --categorizefile=categorizefile.cfg --for-each=day --output-format csv'
}

app.get('/consolidated', (req, res) => {
  exec(arbttShellCommand(), (err, stdout, stderr) => {
    if (err) {
        //some err occurred
        console.error(err)
      console.log(stdout)
      console.log(stderr)
      res.status(502).send(stderr)
    } else {
      var dat = dp.consolidatedData(stdout)
      res.json(dat)
    }
    })


})

app.get('/foreachday', (req, res) => {
  exec(arbttShellCommand(), (err, stdout, stderr) => {
    if (err) {
        //some err occurred
        console.error(err)
      console.log(stdout)
      console.log(stderr)
      res.status(502).send(stderr)
    } else {
    // the *entire* stdout and stderr (buffered)
    //console.log(`stdout: ${stdout}`);
    //console.log(`stderr: ${stderr}`);
      var tableData = dp.tabularData(stdout)
    //console.log(JSON.stringify(tableData,null,2))
    // Send the response body as "Hello World"
      res.json(tableData)
    }
    })

})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
