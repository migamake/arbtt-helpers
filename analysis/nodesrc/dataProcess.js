exports.processData = function processData(data){
  var array = data.split("\n").map(x=>x.split(","))
  var dict = {}
  array.forEach((x)=>{
    var head = x[0]
    var val  = x[1]
    if(dict[head]==undefined){
      dict[head] = [val]
    }
    else{
      dict[head] = dict[head]+val
    }
  })
  return dict
};

exports.tabularData = function tabularData(data){
  var lines = data.split("\n")
  var list = []
  lines.forEach((x)=>{
    var splitVal = x.split(",")
    if(splitVal.length >= 3){
      var date = splitVal[0]
      var pname = splitVal[1]
      var time = splitVal[2]
      list.push({'date':date,'pname':pname,'time':time})
    }
  })
  return list
};

function convertMapToJson(map){
  var array = []
  for(const [key,value] of map.entries()){
    array.push({"date":key,"content":value})
  }
  return array
}
exports.consolidatedData = function consolidatedData(data){
  var lines = data.split("\n")
  let map = new Map()
  lines.forEach((x)=>{
    var csvRow = x.split(",")
    if(csvRow.length >= 3){
        var date = csvRow[0]
        var actualDate = Date.parse(date)
        if(actualDate != NaN){
            var pname = csvRow[1]
            var time = csvRow[2]
            if(map.has(date)){
                var cur = map.get(date)
            map.set(date,[...cur,{"pname":pname,"time":time}])
            }
            else{
            map.set(date,[{"pname":pname,"time":time}])
            }

        }
    }
  })
  return convertMapToJson(map)
};
