module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D

-- custom imports
import View.View exposing (..)
import Types.Types exposing (..)
import Update.Update exposing (..)


init : () -> ( Model, Cmd Msg )
init _ =
    ( { mappedDataRows = []
      , errorMessage = Nothing
      , filterTitleValue = ""
      }
    , Cmd.none
    )


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
