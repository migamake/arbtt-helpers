module Types.Types exposing(..)

import Http


type alias Model =
    { mappedDataRows : List MappedRow
    , errorMessage : Maybe String
    , filterTitleValue : String
    }

type alias Row =
    {
        date: String
        ,pname: String
        ,time: String
    }

type alias MappedRow =
    {
        mappeddate: String
        ,content: List MappedRowContentRow
    }

type alias MappedRowContentRow =
    {
        pname : String
        ,time : String
    }

type Msg
    = SendHttpRequest
    | MappedJsonReceived (Result Http.Error (List MappedRow))
    | FilterTitleChanged String
