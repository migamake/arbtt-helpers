module Update.Update exposing(..)

import Http
import Json.Decode as D

-- custom imports
import Types.Types exposing(..)

url : String
url =
    "http://localhost:3000"


getMappedJson : Cmd Msg
getMappedJson =
    Http.get
        {
            url = url++"/consolidated"
            ,expect = Http.expectJson MappedJsonReceived
                (D.list
                     (D.map2 MappedRow
                          (D.field "date" D.string)
                          (D.field "content"
                           (D.list
                                (D.map2 MappedRowContentRow
                                    (D.field "pname" D.string)
                                    (D.field "time" D.string)
                                )
                           )
                          )
                     )

                )
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SendHttpRequest ->
            ( model, getMappedJson )

        MappedJsonReceived (Err httpError) ->
            ( { model
                | errorMessage = Just (buildErrorMessage "json" httpError)
              }
            , Cmd.none
            )

        MappedJsonReceived (Ok rows) ->
            ({model|mappedDataRows = rows}, Cmd.none)

        FilterTitleChanged s ->
            ({model | filterTitleValue = s }, Cmd.none)



buildErrorMessage : String -> Http.Error -> String
buildErrorMessage s httpError =
    case httpError of
        Http.BadUrl message ->
            message

        Http.Timeout ->
            s++" Server is taking too long to respond. Please try again later."

        Http.NetworkError ->
            s++" Unable to reach server."

        Http.BadStatus statusCode ->
            s++" Request failed with status code: " ++ String.fromInt statusCode

        Http.BadBody message ->
            s++" "++message
