module View.Query exposing (queryDiv)

import List exposing (length,head,repeat,concat)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick,onInput)

import Types.Types exposing (..)
import View.Style exposing (..)

queryDiv : Html Msg
queryDiv = div [] [
                  table queryTableStyle [

                       -- header
                       thead []
                           [
                            th queryTableHeaderStyle  [text "name"]
                           ,th queryTableHeaderStyle  [text "value"]
                           ]

                      -- To filter by date
                      ,tr    []
                           [
                            td queryTableRowStyle [text "date"]
                           ,td queryTableRowStyle [input [type_ "date"] []]
                           ]

                      -- To filter by pname
                      ,tr    []
                           [
                            td queryTableRowStyle [text "title"]
                           ,td queryTableRowStyle [input [type_ "text", onInput FilterTitleChanged] []]
                           ]

                     -- end
                      ]
                  ]
