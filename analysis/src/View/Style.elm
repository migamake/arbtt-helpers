module View.Style exposing(..)

import Html exposing (..)
import Html.Attributes exposing(style)

mainDivStyle : List (Attribute msg)
mainDivStyle =
    [
     style "align" "middle"
    ,style "margin" "50px"
    ]


-- Query space style

buttonStyle : List (Attribute msg)
buttonStyle =
    [ style "width" "250px"
    , style "background-color" "#397cd5"
    , style "color" "white"
    , style "padding" "14px 5px"
    , style "margin-top" "10px"
    , style "border" "none"
    , style "border-radius" "20px"
    , style "font-size" "16px"
    ]

queryDivStyle : List (Attribute msg)
queryDivStyle =
    [
     style "align" "middle"
    ,style "border-radius" "25px"
    ,style "border" "2px solid #73AD21"
    ,style "padding" "20px"
    ,style "margin" "20px"
    ]

queryTableStyle : List (Attribute msg)
queryTableStyle =
    [
    style "border" "2px solid black"
    ,style "border-collapse" "collapse"
    ,style "marginLeft" "20px"
    ,style "margin" "20px"
    ]

queryTableHeaderStyle : List (Attribute msg)
queryTableHeaderStyle =
    [
    style "background" "#000"
    ,style "padding" "10px"
    ,style "color" "white"
    ]

queryTableRowStyle : List (Attribute msg)
queryTableRowStyle =
    [
    style "padding" "10px"
    ,style "border" "1px solid grey"
    ]



-- Display Stuff

displayDivStyle : List (Attribute msg)
displayDivStyle =
    [
     style "align" "middle"
    ,style "border-radius" "25px"
    ,style "border" "2px solid #ed5a5a"
    ,style "padding" "20px"
    ,style "margin" "20px"
    ]

displayTableStyle : List (Attribute msg)
displayTableStyle =
    [
    style "border" "2px solid black"
    ,style "border-collapse" "collapse"
    ,style "marginLeft" "auto"
    ,style "marginRight" "auto"
    ]

displayTableHeaderStyle : List (Attribute msg)
displayTableHeaderStyle =
    [
    style "background" "#000"
    ,style "padding" "10px"
    ,style "color" "white"
    ]

displayTableRowStyle : List (Attribute msg)
displayTableRowStyle =
    [
    style "padding" "10px"
    ,style "border" "1px solid grey"
    ]
