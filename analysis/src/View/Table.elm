module View.Table exposing (createCombinationTable,convertToCombinationTable)

import List exposing (length,head,repeat,concat)
import String exposing (contains)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import Types.Types exposing (..)
import View.Style exposing (..)

type alias TableType =
    { tableRowsData : List TableRowType
    , tableHeaderData : List String
    , tableMetaData : Maybe String
    , tableTotalColumns : Int
    }

type TableRowType
    = SimpleRows Int (List String)
    | HugeColumn Int String (List TableRowType)

createCombinationTable : TableType -> Html Msg
createCombinationTable tableData =
    let
        printSingleHeader : String -> Html Msg
        printSingleHeader singleHead = th displayTableHeaderStyle [text singleHead]

{-
        printRow : (List String) -> Html Msg
        printRow cells = tr [] (List.map (\x -> td displayTableRowStyle [text x]) cells)

        convertToRows : List TableRowType -> List (List String)
        convertToRows x =[[]]
-}
        printTableRow : TableRowType -> List (Html Msg)
        printTableRow row = case row of
            SimpleRows rowsBefore cells -> [tr [] (List.map (\x -> td displayTableRowStyle [text x]) cells)]
            HugeColumn rowsBefore cellName cells ->
                [tr [] [td (displayTableRowStyle++[rowspan ((length cells)+1)]) [text cellName]]]
                ++List.concat (List.map printTableRow cells)
    in
        table displayTableStyle
            (
            [ thead [] (List.map printSingleHeader tableData.tableHeaderData)
--            ] ++ (List.map printRow (convertToRows tableData.tableRowsData))
            ] ++ List.concat (List.map printTableRow tableData.tableRowsData)
            )

removeNothingFromList : List (Maybe a) -> List a
removeNothingFromList list =
    List.filterMap identity list

forAday : List MappedRowContentRow -> String -> List TableRowType
forAday mcr titleString = removeNothingFromList ( List.map (\x -> case contains titleString x.pname of
                                              True -> Just (SimpleRows 0 [x.pname,x.time])
                                              False -> Nothing
                                   ) mcr
                                                      )


convertToCombinationTable : List MappedRow -> String -> TableType
convertToCombinationTable data titleString
                              = TableType
                                 -- Rows
                                (List.map (\x-> (HugeColumn 0 x.mappeddate (forAday x.content titleString) )) data)
                                 -- Headers
                                ["Date","Program","Time"]
                                 -- meta
                                Nothing
                                 -- num columns
                                0


putMappedTable : List MappedRow -> Html Msg
putMappedTable data =
    let
        dateContentLister = (\x -> tr [] [
                                                 td displayTableRowStyle [text x.pname]
                                                ,td displayTableRowStyle [text x.time ]
                                                ])
        lister : List MappedRow -> List (Html Msg)
        lister val = List.concat (List.map printRow val)
        printRow x =
            let
                totalContent = List.map dateContentLister x.content
            in
                [
                            tr [] [
                td (displayTableRowStyle++[rowspan ((length x.content)+1)]) [text x.mappeddate]
                ,td displayTableRowStyle [text ""]
                ,td displayTableRowStyle [text ""]
                ]
                                ]++totalContent

    in
        table displayTableStyle
        ([ thead []
              [
                 th displayTableHeaderStyle [text "Date"]
                ,th displayTableHeaderStyle [text "Program"]
                ,th displayTableHeaderStyle [text "Time"]
              ]
        ]++lister data)
