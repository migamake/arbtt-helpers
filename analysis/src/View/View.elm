module View.View exposing (view)

import List exposing (length)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import Types.Types exposing (..)
import View.Style exposing (..)
import View.Table exposing (..)
import View.Query exposing (..)

view : Model -> Html Msg
view model =
    div mainDivStyle [
        div queryDivStyle
            [ queryDiv
             ,button ([ onClick SendHttpRequest ]++buttonStyle)
                [ text "Get arbtt data from server" ]
            ]
        ,div displayDivStyle
            [
            viewDataOrError model
            ]
    ]


viewDataOrError : Model -> Html Msg
viewDataOrError model =
    case model.errorMessage of
        Just message ->
            viewError message

        Nothing ->
            viewMappedData model.mappedDataRows model.filterTitleValue


viewError : String -> Html Msg
viewError errorMessage =
    let
        errorHeading =
            "Couldn't fetch nicknames at this time."
    in
    div []
        [ h3 [] [ text errorHeading ]
        , text ("Error: " ++ errorMessage)
        ]

viewMappedData : List MappedRow -> String -> Html Msg
viewMappedData data titleString =
    div []
        [
         case data of
             [] -> h3 [] [text "Press button to begin"]
             x  -> createCombinationTable (convertToCombinationTable data titleString)
        ]


