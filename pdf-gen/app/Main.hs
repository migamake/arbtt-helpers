module Main where

import           Control.Monad                 (forM_)
import           Data.CSV
import           Data.Char
import           Data.List
import           System.Environment
import           System.IO
import           System.Process
import           Text.JSON.Generic
import           Text.ParserCombinators.Parsec

import           CodeGen.PDFGen
import           Types

import           Data.List.Split               (splitOn)
import qualified Data.Map                      as Map
import           Data.Maybe                    (fromMaybe)
import           GHC.IO.IOMode                 (IOMode (WriteMode))
import           Language.Haskell.TH.PprLib    (text)


parseArgs :: [String] -> Args
parseArgs (x:xs) = case splitOn "=" x of
  ["-f"]         -> (parseArgs xs){recalculate = True}
  ["-pdf"]       -> (parseArgs xs){createPDF  = True}
  ["--d",d]      -> (parseArgs xs){date = Just d}
  ["--s",file]   -> (parseArgs xs){settingsFile = Just file}
  ["--c",cfiles] -> (parseArgs xs){captureFile = Just $ splitOn ";" cfiles}
  ["--invoiceNumber",inumber] -> (parseArgs xs){invoiceNumber = Just inumber}
  _              ->  parseArgs xs
parseArgs [] = Args{
  recalculate  = False
  ,createPDF  = False
  ,invoiceNumber = Nothing
  ,date = Nothing
  ,corrected = False
  ,settingsFile = Nothing
  ,captureFile = Nothing
                        }

convertToShellCommands :: Args -> [String]
convertToShellCommands args = case captureFile args of
  Nothing -> ["arbtt-stats --for-each=day --output-format csv >> data.csv"]
  Just cfiles -> map (\x ->
                        "arbtt-stats --for-each=day --output-format csv --logfile="++x++" >> data.csv") cfiles

main :: IO ()
main = do
  -- get arguments and environment variables
  args <- getArgs
  let parsedArgs = parseArgs args
  print parsedArgs

  -- clearing previous data
  exit <- system "rm data.csv"

  -- getting status
  forM_ (convertToShellCommands parsedArgs) system
  -- exit <- system "arbtt-stats --for-each=day --output-format csv > data.csv"

  -- Reading CSV data
  csvData <- parseFromFile csvFile "data.csv"
  print $ parseCSV csvData parsedArgs

  -- Reading JsonData
  handle <- openFile (fromMaybe "settings.json" (settingsFile parsedArgs)) ReadMode
  contents <- hGetContents handle
  print (decodeJSON contents :: PersonalInfo)

  -- writing to invoices.text
  exit <- system "rm invoices/invoices*"
  writeFile "invoices/invoices.tex" $ pdfcode parsedArgs (decodeJSON contents :: PersonalInfo) $ parseCSV csvData parsedArgs
  exit <- system "cd invoices; pdflatex invoices.tex > /dev/null"

  -- exit
  putStrLn "Concluding.."
