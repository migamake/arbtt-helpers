{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TemplateHaskell    #-}

module CodeGen.Hardcoded where

import           Data.Char
import           Data.Generics
import qualified Data.Text                   as T
import           Language.Haskell.TH
import qualified Language.Haskell.TH         as TH
import           Language.Haskell.TH.Quote
import           Language.Haskell.TH.Syntax

import           Language.Haskell.Meta.Parse

import           Text.Parsec


-- | Define which patterns are accepted as variable declaration boundaries.
variablePatterns :: [(String, String)]
variablePatterns =
  [ ("{#", "#}")   -- Original (old) variable identifier.
  , ("//#{", "}")  -- Commented Shakespearean pattern for Scala/Rust.
  , ("-- #{", "}") -- Commented Shakespearean pattern for Haskell.
  , ("#{", "}")    -- Imitating Shakespearean (HTML template language for Haskell) patterns.
  ]

-- | Replaces a list of diverse (Token Identifier, Replacement String)
-- in a single String.
replaceTokensOnFile :: [(String, String)] -> String -> String
replaceTokensOnFile tokenReplacements fileContents =
  T.unpack
  $ foldl replaceToken (T.pack fileContents) tokenReplacements

replaceToken :: T.Text -> (String, String) -> T.Text
replaceToken t (token_id, replacement) = foldl replaceSingleBound t variablePatterns
  where
    buildInputPattern (vO, vC)   = vO ++ token_id ++ vC
    replaceSingleBound t' bounds = T.replace (T.pack $ buildInputPattern bounds) (T.pack replacement) t'

-- | Main parser for the 'code' QuasiQuoter.
expr    :: Parsec String () Expr
expr    =  try parseVar <|> parseSegment

parseSegment :: Parsec String () Expr
parseSegment = many1 content
           >>= (return . PlainText)
  where
    content = notFollowedBy openers
           >> anyChar
    openers = choice
            $ map (string . fst) variablePatterns

-- | Parses variables formatted according to pattern boundaries
-- defined by `variablePatterns`.
parseVar :: Parsec String () Expr
parseVar = Expression <$> choice (map singlePattern variablePatterns)

  where
    singlePattern (vO, vC) = string vO
                          *> manyTill anyChar (string vC)

parseExprs :: MonadFail m => String -> m [Expr]
parseExprs s =
    case runParser (many1 expr) () "" s of
      Left err -> fail $ show err
      Right e  -> return e

data Expr  = PlainText  String
           | Expression String
 deriving (Show, Typeable, Data)

-- | TODO: Possible improvements here:
-- 1. Detect if (Variable v) corresponds to a Text instance,
-- then convert it to String (using T.unpack) automatically if that's the case.
-- Use TH.lookupTypeName on this?
-- 2. [DONE] Allow (Variable v) to hold a function call or expression instead of just variable names.
-- 3. Allow generic Haskell expressions as variables for more expressivity.
-- 4. Remove indendation from all lines of the `code` output.
-- This allows for nicer formatting when declaring individual `code` segments,
-- as it will respect Haskell's natural code indendation.
antiExprExp :: Expr -> Q Exp
antiExprExp  (PlainText  v)     = litE   $ StringL v
antiExprExp  (Expression v)     = return $ gotExp v
  where
    gotExp = either undefined id . parseExp

readVarE :: String -> Q Exp
readVarE = TH.varE . TH.mkName

concatExpressions :: [Expr] -> Q Exp
concatExpressions = foldr1 (\ e e' -> infixApp e [| (++) |] e')
                  . map antiExprExp

checkHeadAndTail :: [Expr] -> [Expr]
checkHeadAndTail xs =
  case len > 0 of
    True  -> zipWith ($) functionMaps xs
    False -> xs
  where
    len          = length xs - 2
    functionMaps = [filterNewLine trimChars]
                ++ replicate len id
                ++ [filterNewLine (reverse . trimChars . reverse)]
    trimChars    = dropWhile isSpace
                 . dropWhile (== '\n')

-- | Allows to ignore leading and trailing
-- newlines in literal string blocks.
filterNewLine :: (String -> String) -> Expr -> Expr
filterNewLine f (PlainText v) = PlainText $ f v
filterNewLine _ e             = e

-- | Converts the raw string extracted from the `QuasiQuoter`
-- to the full equivalent `Q Exp`.
codeFromString :: String -> Q Exp
codeFromString s =
  parseExprs s
  >>= dataToExpQ (const Nothing `extQ` (Just . concatExpressions . checkHeadAndTail))

-- | `QuasiQuoter` for foreign code parsing.
code :: QuasiQuoter
code =  QuasiQuoter
  { quoteExp  = codeFromString
  , quotePat  = undefined
  , quoteType = undefined
  , quoteDec  = undefined
  }
