{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE RecordWildCards #-}
module CodeGen.PDFGen where
import           CodeGen.Hardcoded
import           Data.List.Split
import qualified Data.Map                      as Map
import           Data.Maybe                    (fromMaybe)
import           Data.Text                     (isInfixOf, pack)
import qualified Data.Text                     as T
import           Data.Time
import           Text.ParserCombinators.Parsec
import           Text.Printf
import           Types

parseCSV :: Either ParseError [[String]] -> Args -> Map.Map String Float
parseCSV (Left _)  args = error "Parsing failed.."
parseCSV (Right x) args = totalTime args $ tail x

convertTime :: String -> Float
convertTime x = hour + ( minute / 60 )
  where
    timeList = splitOn ":" x
    hour = read $ head timeList
    minute = read $ head $ tail timeList

shouldConsider :: String -> (Maybe String,String) -> Bool
shouldConsider str dt = dateMatch dt && any (\word -> isInfixOf word  $ pack str ) words
  where
    words = map pack ["Emacs:","Migamake:","Haskell:","Terminal:"]
    dateMatch (Nothing,x) = True
    dateMatch (Just today,x) = (tail $ splitOn "-" today) == (tail $ reverse $ splitOn "-" x)

totalTime :: Args -> [[String]] -> Map.Map String Float
totalTime args (x:xs) =
  if shouldConsider program (Types.date args,date) then
    case Map.lookup date finMap of
      Just t  -> Map.insert date (t +time ) finMap
      Nothing -> Map.insert date time finMap
  else
    finMap

  where
    date = head x
    program = head $ tail x
    time = convertTime $ head $ tail $ tail x
    finMap = totalTime args xs
totalTime args [] = Map.empty

pdfcode :: Args -> PersonalInfo -> Map.Map String Float -> String
pdfcode comArgs personalInfo@PersonalInfo{..} sumData =
  [code|
\documentclass{invoice}

\def \tab {\hspace*{3ex}}

\begin{document}

\hfil{\Huge\bf Invoice-#{inum} }\hfil
\bigskip\break
\hrule
#{T.unpack name} \hfill invoice-#{inum} \\
#{T.unpack addressLine1} \hfill #{T.unpack phoneNum} \\
#{T.unpack addressLine2} \hfill #{T.unpack emailID} \\
#{T.unpack addressLine3}
\\ \\
{\bf Banking Info: \hfill Currency Info:}\\
\tab Bank Name : #{T.unpack bankName} \hfill Hourly Rate: #{T.unpack hourlyRate} \\
\tab Account Holder Name : #{T.unpack accountHolderName} \hfill Exchange Rate: #{T.unpack exchangeRate} \\
\tab Bank A/c no. : #{T.unpack bankACNum} \\
\tab IFSC Code : #{T.unpack ifscCode} \\
{\bf Invoice To:} \\
\tab #{T.unpack toName} \\
\tab #{T.unpack toAddrLine1} \\
\tab #{T.unpack toAddrLine2} \\
\tab #{T.unpack toAddrLine3} \\

{\bf Date:} \\
\tab \today \\

\hourlyrate{846}

\begin{invoiceTable}
\feetype{Consulting Services}
#{invoice_data}
\subtotal
\end{invoiceTable}

\end{document}
       |] ++ "\n"
    where
      inum = fromMaybe "00000" (invoiceNumber comArgs)
      invoice_data = unlines $ map toRow $ Map.toList sumData
      toRow (s,f) = "\\hourrow{"++s++"}"++"{"++printf "%0.2f" f++"}"
