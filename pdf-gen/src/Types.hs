{-# LANGUAGE DeriveDataTypeable #-}

module Types where

import           Data.Text
import           Text.JSON.Generic

data Args = Args{
                 recalculate   :: Bool
                ,createPDF     :: Bool
                ,invoiceNumber :: Maybe String
                ,date          :: Maybe String
                ,corrected     :: Bool
                ,settingsFile  :: Maybe String
                ,captureFile   :: Maybe [String]
                }
            deriving Show

data PersonalInfo = PersonalInfo{
  name               :: !Text
  ,addressLine1      :: !Text
  ,addressLine2      :: !Text
  ,addressLine3      :: !Text
  ,bankName          :: !Text
  ,accountHolderName :: !Text
  ,bankACNum         :: !Text
  ,ifscCode          :: !Text
  ,phoneNum          :: !Text
  ,emailID           :: !Text
  ,toName            :: !Text
  ,toAddrLine1       :: !Text
  ,toAddrLine2       :: !Text
  ,toAddrLine3       :: !Text
  ,hourlyRate        :: !Text
  ,exchangeRate      :: !Text
                                }
                    deriving (Show, Data, Typeable)
